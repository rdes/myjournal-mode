;;; jrnl.el --- jrnl: functions for my journal mode
;;; Author: Ryan Desfosses <ryan@desfo.org>

;;; Commentary:

;;; Code:
(defun jrnl-today ()
  "Create journal entry for today."
  (interactive)
  (beginning-of-line)
  (insert (format-time-string "* %F %a")))

(defun jrnl-tomorrow ()
  "Create journal entry for tomorrow."
  (interactive)
      (switch-to-buffer (make-temp-name "jrnl"))
      (insert (format-time-string "[%F %a]"))
      (beginning-of-line)
      (org-timestamp-up-day)
      (replace-string "[" "* ")
      (replace-string "]" "")
      (copy-rectangle-to-register 'jrnl 1 (+ 1 (buffer-size)))
      (switch-to-buffer (other-buffer))
      (insert-register 'jrnl)
      (kill-buffer (other-buffer))
      )

(defun jrnl-goto-today ()
  "Jump to days journal entry."
  (interactive)
  (search-backward (format-time-string "* %F %a") nil t)
  (search-forward (format-time-string "* %F %a") nil nil)
  )

(defun jrnl-goto-yesterday ()
  "Jump to yesterdays journal entry."
  (interactive)
    (switch-to-buffer (make-temp-name "jrnl"))
      (insert (format-time-string "[%F %a]"))
      (beginning-of-line)
      (org-timestamp-down-day)
      (replace-string "[" "* ")
      (replace-string "]" "")
      (setq yday (buffer-substring 1 (+ 1 (buffer-size))))
      (switch-to-buffer (other-buffer))
      (kill-buffer (other-buffer))
      (progn
      (message yday)
      (search-backward yday nil t)
      (search-forward yday nil nil))
      )

(defun jrnl-goto-tomorrow ()
  "Jump to yesterdays journal entry."
  (interactive)
     (switch-to-buffer (make-temp-name "jrnl"))
      (insert (format-time-string "[%F %a]"))
      (beginning-of-line)
      (org-timestamp-up-day)
      (replace-string "[" "* ")
      (replace-string "]" "")
      (setq yday (buffer-substring 1 (+ 1 (buffer-size))))
      (switch-to-buffer (other-buffer))
      (kill-buffer (other-buffer))
      (progn
      (message yday)
      (search-backward yday nil t)
      (search-forward yday nil nil))
      )

;; maybe add feature to place the date in the correct location
(defun jrnl-next-monday ()
  "Insert date of next Monday"
  (interactive)
  (let* ((tday (format-time-string "%a"))
         (ndays (pcase tday
                  ("Sun" 1)
                  ("Mon" 7)
                  ("Tue" 6)
                  ("Wed" 5)
                  ("Thu" 4)
                  ("Fri" 3)
                  ("Sat" 2))))
    (switch-to-buffer (make-temp-name "jrnl"))
    (insert (format-time-string "[%F %a]"))
    (beginning-of-line)
    (dotimes (r ndays) (org-timestamp-up-day))
    (replace-string "[" "* ")
    (replace-string "]" "")
    (copy-to-register 'jrnl 1 (+ 1 (buffer-size)))
    (switch-to-buffer (other-buffer))
    (insert-register 'jrnl)
    (kill-buffer (other-buffer))))

(defun jrnl-next-tuesday ()
  "Insert date of next Tuesday"
  (interactive)
  (let* ((tday (format-time-string "%a"))
         (ndays (pcase tday
                  ("Sun" 2)
                  ("Mon" 1)
                  ("Tue" 7)
                  ("Wed" 6)
                  ("Thu" 5)
                  ("Fri" 4)
                  ("Sat" 3))))
    (switch-to-buffer (make-temp-name "jrnl"))
    (insert (format-time-string "[%F %a]"))
    (beginning-of-line)
    (dotimes (r ndays) (org-timestamp-up-day))
    (replace-string "[" "* ")
    (replace-string "]" "")
    (copy-to-register 'jrnl 1 (+ 1 (buffer-size)))
    (switch-to-buffer (other-buffer))
    (insert-register 'jrnl)
    (kill-buffer (other-buffer))))

(defun jrnl-next-wednesday ()
  "Insert date of next Wednesday"
  (interactive)
  (let* ((tday (format-time-string "%a"))
         (ndays (pcase tday
                  ("Sun" 3)
                  ("Mon" 2)
                  ("Tue" 1)
                  ("Wed" 7)
                  ("Thu" 6)
                  ("Fri" 5)
                  ("Sat" 4))))
    (switch-to-buffer (make-temp-name "jrnl"))
    (insert (format-time-string "[%F %a]"))
    (beginning-of-line)
    (dotimes (r ndays) (org-timestamp-up-day))
    (replace-string "[" "* ")
    (replace-string "]" "")
    (copy-to-register 'jrnl 1 (+ 1 (buffer-size)))
    (switch-to-buffer (other-buffer))
    (insert-register 'jrnl)
    (kill-buffer (other-buffer))))

(defun jrnl-next-thursday ()
  "Insert date of next Thursday"
  (interactive)
  (let* ((tday (format-time-string "%a"))
         (ndays (pcase tday
                  ("Sun" 4)
                  ("Mon" 3)
                  ("Tue" 2)
                  ("Wed" 1)
                  ("Thu" 7)
                  ("Fri" 6)
                  ("Sat" 5))))
    (switch-to-buffer (make-temp-name "jrnl"))
    (insert (format-time-string "[%F %a]"))
    (beginning-of-line)
    (dotimes (r ndays) (org-timestamp-up-day))
    (replace-string "[" "* ")
    (replace-string "]" "")
    (copy-to-register 'jrnl 1 (+ 1 (buffer-size)))
    (switch-to-buffer (other-buffer))
    (insert-register 'jrnl)
    (kill-buffer (other-buffer))))

(defun jrnl-next-friday ()
  "Insert date of next Friday"
  (interactive)
  (let* ((tday (format-time-string "%a"))
         (ndays (pcase tday
                  ("Sun" 5)
                  ("Mon" 4)
                  ("Tue" 3)
                  ("Wed" 2)
                  ("Thu" 1)
                  ("Fri" 7)
                  ("Sat" 6))))
    (switch-to-buffer (make-temp-name "jrnl"))
    (insert (format-time-string "[%F %a]"))
    (beginning-of-line)
    (dotimes (r ndays) (org-timestamp-up-day))
    (replace-string "[" "* ")
    (replace-string "]" "")
    (copy-to-register 'jrnl 1 (+ 1 (buffer-size)))
    (switch-to-buffer (other-buffer))
    (insert-register 'jrnl)
    (kill-buffer (other-buffer))))

(defun jrnl-next-saturday ()
  "Insert date of next Saturday"
  (interactive)
  (let* ((tday (format-time-string "%a"))
         (ndays (pcase tday
                  ("Sun" 6)
                  ("Mon" 5)
                  ("Tue" 4)
                  ("Wed" 3)
                  ("Thu" 2)
                  ("Fri" 1)
                  ("Sat" 7))))
    (switch-to-buffer (make-temp-name "jrnl"))
    (insert (format-time-string "[%F %a]"))
    (beginning-of-line)
    (dotimes (r ndays) (org-timestamp-up-day))
    (replace-string "[" "* ")
    (replace-string "]" "")
    (copy-to-register 'jrnl 1 (+ 1 (buffer-size)))
    (switch-to-buffer (other-buffer))
    (insert-register 'jrnl)
    (kill-buffer (other-buffer))))

(defun jrnl-next-sunday ()
  "Insert date of next Sunday"
  (interactive)
  (let* ((tday (format-time-string "%a"))
         (ndays (pcase tday
                  ("Sun" 7)
                  ("Mon" 6)
                  ("Tue" 5)
                  ("Wed" 4)
                  ("Thu" 3)
                  ("Fri" 2)
                  ("Sat" 1))))
    (switch-to-buffer (make-temp-name "jrnl"))
    (insert (format-time-string "[%F %a]"))
    (beginning-of-line)
    (dotimes (r ndays) (org-timestamp-up-day))
    (replace-string "[" "* ")
    (replace-string "]" "")
    (copy-to-register 'jrnl 1 (+ 1 (buffer-size)))
    (switch-to-buffer (other-buffer))
    (insert-register 'jrnl)
    (kill-buffer (other-buffer))))

;; Code for 'jrnl-run-src-block' was found here
;; https://emacs.stackexchange.com/questions/32761/execute-named-org-bable-source-block-from-elisp
;; thanks mutbuerger
(defun jrnl-run-src-block ()
  "Shows list of named source blocks that can be run"
  (interactive)
  (save-excursion
    (goto-char
     (org-babel-find-named-block
      (completing-read "Code Block: " (org-babel-src-block-names))))
    (org-babel-execute-src-block-maybe)))

(define-minor-mode jrnl
  "provides few useful functions"
  :lighter " jrnl"
  )

(add-hook 'org-mode-hook 'jrnl)

(provide 'jrnl)

;;; myjournal-mode ends here
